package selection;

import java.util.Random;

import core.Individual;
import core.Population;

public class TournamentSelection extends AbstractSelectionOperator {
	private final int tournamentSize;
	private final Random r = new Random(System.nanoTime());

	public TournamentSelection(Population population, int tournamentSize) {
		super(population);
		this.tournamentSize = tournamentSize;
	}

	@Override
	public Individual select(Individual alreadySelected) {

		Individual fittestCandidate = null;
		for (int i = 0; i < tournamentSize;) {
			Individual candidate = population.getCandidates().get(
					r.nextInt(population.getCandidates().size()));
			if (!candidate.equals(alreadySelected)) {
				if (fittestCandidate == null
						|| fittestCandidate.getFitness().getOverallFitness() > candidate
								.getFitness().getOverallFitness())
					fittestCandidate = candidate;
				i++;
			}
		}
		return fittestCandidate;
	}
}
