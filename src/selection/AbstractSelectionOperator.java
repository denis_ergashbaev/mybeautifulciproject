package selection;

import core.Individual;
import core.Population;

public abstract class AbstractSelectionOperator {

	protected final Population population;

	public AbstractSelectionOperator(Population population) {
		this.population = population;
	}

	public abstract Individual select(Individual alreadySelected);
}
