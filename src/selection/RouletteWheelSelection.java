package selection;

import java.util.List;
import java.util.Random;

import core.Individual;
import core.Population;

public class RouletteWheelSelection extends AbstractSelectionOperator {
	private final Random r = new Random(System.nanoTime());

	public RouletteWheelSelection(Population population) {
		super(population);
	}

	@Override
	public Individual select(Individual alreadySelected) {
		double fitnessSum = population.getFitnessSum();
		fitnessSum = fitnessSum
				- population.getPopulationFittestIndividual().getFitness()
						.getOverallFitness();
		List<Individual> candidates = population.getSortedCandidates();

		double to = r.nextDouble() * fitnessSum;
		double p = fitnessSum;
		int pick = 0;
		for (int i = 1; p > to; i++) {
			if (candidates.get(i).equals(alreadySelected)) {
				break;
			}
			p -= candidates.get(i).getFitness().getOverallFitness();
			pick = i;
		}

		return candidates.get(pick);
	}

}
