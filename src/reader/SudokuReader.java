package reader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import core.SquareLineConverter;

public class SudokuReader {

	public static List<List<Integer>> read(String sudokuFileName) {
		List<Integer> allNumbers = Lists.newArrayList();
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(sudokuFileName));
			for (String line = null; (line = br.readLine()) != null;) {
				for (String s : Splitter.on(",").trimResults().split(line)) {
					allNumbers.add(Integer.valueOf(s));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) {
					br.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return SquareLineConverter.convert(allNumbers);
	}
}
