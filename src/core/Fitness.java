package core;

import java.util.List;

import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;

public class Fitness {

	private final double overallFitness;
	private final int arithmeticSumRows;
	private final int arithmeticSumCols;
	private final double geometricProductRows;
	private final double geometricProductCols;
	private final int numbersMissingRows;
	private final int numbersMissingCols;
	private final List<Integer> arithmeticSumRowsSegment;
	private final List<Integer> arithmeticSumColsSegment;
	private final int minConsecutiveRowsBlocks;
	private final int minConsecutiveColBlocks;

	public Fitness(List<Integer> arithmeticSumRowsList,
			List<Integer> arithmeticSumColsList,
			List<Double> geometricProductRowsList,
			List<Double> geometricProductColsList, int numRowsMissing,
			int numColsMissing) {
		this.arithmeticSumRowsSegment = sumIntegerSegments(arithmeticSumRowsList);
		this.arithmeticSumRows = sumInteger(arithmeticSumRowsList);
		this.arithmeticSumColsSegment = sumIntegerSegments(arithmeticSumColsList);
		this.arithmeticSumCols = sumInteger(arithmeticSumColsList);
		this.geometricProductRows = sumDouble(geometricProductRowsList);
		this.geometricProductCols = sumDouble(geometricProductColsList);
		this.numbersMissingRows = numRowsMissing;
		this.numbersMissingCols = numColsMissing;
		this.minConsecutiveRowsBlocks = Ordering.<Integer> natural().min(
				arithmeticSumRowsSegment);
		this.minConsecutiveColBlocks = Ordering.<Integer> natural().min(
				arithmeticSumColsSegment);
		double overallFitness = 10
				* (this.arithmeticSumRows + this.arithmeticSumCols) + 50
				* (numRowsMissing + numColsMissing)
				+ (this.geometricProductRows + this.geometricProductCols) + 0
				* (minConsecutiveRowsBlocks + minConsecutiveColBlocks);
		this.overallFitness = overallFitness;
	}

	private int sumInteger(List<Integer> list) {
		int retVal = 0;
		for (int l : list) {
			retVal += l;
		}
		return retVal;
	}

	private List<Integer> sumIntegerSegments(List<Integer> list) {
		List<Integer> segmentsSum = Lists.newArrayList();
		for (int i = 0; i < 3; i++) {
			int start = i * 3;
			segmentsSum.add(sumInteger(list.subList(start, start + 3)));
		}
		return segmentsSum;
	}

	private double sumDouble(List<Double> list) {
		double retVal = 0;
		for (double l : list) {
			retVal += l;
		}
		return retVal;
	}

	public List<Integer> getArithmeticSumRowsSegment() {
		return arithmeticSumRowsSegment;
	}

	public List<Integer> getArithmeticSumColsSegment() {
		return arithmeticSumColsSegment;
	}

	public double getOverallFitness() {
		return overallFitness;
	}

	public boolean isConverged() {
		return getOverallFitness() == 0;
	}

	@Override
	public String toString() {
		return "Fitness [overallFitness=" + overallFitness
				+ ", arithmeticSumRows=" + arithmeticSumRows
				+ ", arithmeticSumCols=" + arithmeticSumCols
				+ ", geometricProductRows=" + geometricProductRows
				+ ", geometricProductCols=" + geometricProductCols
				+ ", numbersMissingRows=" + numbersMissingRows
				+ ", numbersMissingCols=" + numbersMissingCols + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + arithmeticSumCols;
		result = prime * result + arithmeticSumRows;
		long temp;
		temp = Double.doubleToLongBits(geometricProductCols);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(geometricProductRows);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + numbersMissingCols;
		result = prime * result + numbersMissingRows;
		temp = Double.doubleToLongBits(overallFitness);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Fitness other = (Fitness) obj;
		if (arithmeticSumCols != other.arithmeticSumCols)
			return false;
		if (arithmeticSumRows != other.arithmeticSumRows)
			return false;
		if (Double.doubleToLongBits(geometricProductCols) != Double
				.doubleToLongBits(other.geometricProductCols))
			return false;
		if (Double.doubleToLongBits(geometricProductRows) != Double
				.doubleToLongBits(other.geometricProductRows))
			return false;
		if (numbersMissingCols != other.numbersMissingCols)
			return false;
		if (numbersMissingRows != other.numbersMissingRows)
			return false;
		if (Double.doubleToLongBits(overallFitness) != Double
				.doubleToLongBits(other.overallFitness))
			return false;
		return true;
	}

}
