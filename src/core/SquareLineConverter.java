package core;

import java.util.List;

import com.google.common.collect.Lists;

public class SquareLineConverter {

	public static List<List<Integer>> convert(List<Integer> allLines) {
		List<List<Integer>> rows = Lists.newArrayList();
		for (int i = 0; i < 3; i++) {
			int startRow = i * 9 * 3;
			for (int j = 0; j < 3; j++) {
				List<Integer> row = Lists.newArrayList();
				int startRow2 = startRow + j * 3;
				for (int k = 0; k < 3; k++) {
					int start = startRow2 + k * 9;
					int end = start + 3;
					row.addAll(allLines.subList(start, end));
				}
				rows.add(row);
			}
		}
		return rows;
	}
}
