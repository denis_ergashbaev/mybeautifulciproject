package core;

import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import logging.LoggerConfigurer;
import reader.SudokuReader;
import selection.AbstractSelectionOperator;
import selection.RouletteWheelSelection;
import selection.TournamentSelection;

import com.google.common.collect.Lists;
import com.google.common.io.Files;

import crossover.AbstractCrossover;
import crossover.BlockwiseCrossover;
import crossover.OnePointCrossover;
import crossover.UniformCrossover;

public class MyBeautifulCIProject {
	private static final Logger logger = Logger
			.getLogger(MyBeautifulCIProject.class.getSimpleName());

	public static void main(String[] args) {
		String sudokuFileName = "test_easy.txt";
		int populationCount = 5000;
		int maxIterations = 500;
		int numberOfElites = 1;
		int tournamentSize = 6;
		double mutationProb = 10;
		int maxReruns = 10;
		// tried, instead using Reruns
		boolean useMutationExplosion = false;
		double maxMutateProb = 50;

		String errorMsg = "The program must be run with the following arguments:\n"
				+ "java -jar myBeautifulCIProject.jar"
				+ "Optional argumens are: "
				+ "-sudokuFileName FILENAME\n"
				+ "-populationCount [1..100]\n"
				+ "-maxIterations [1..100]\n"
				+ "-numberOfElites [1..100]\n"
				+ "-tournamentSize [1..100]\n"
				+ "-mutationProb [1..100]\n" //
				+ "-maxReruns [1..100]\n";
		for (int i = 0; i < args.length; i += 2) {
			String arg = args[i];
			String val = args[i + 1];
			if ("-sudokuFileName".equals(arg)) {
				sudokuFileName = val;
			} else if ("-populationCount".equals(arg)) {
				populationCount = Integer.parseInt(val);
			} else if ("-maxIterations".equals(arg)) {
				maxIterations = Integer.parseInt(val);
			} else if ("-numberOfElites".equals(arg)) {
				numberOfElites = Integer.parseInt(val);
			} else if ("-tournamentSize".equals(arg)) {
				tournamentSize = Integer.parseInt(val);
			} else if ("-mutationProb".equals(arg)) {
				mutationProb = Double.parseDouble(val);
			} else if ("-maxReruns".equals(arg)) {
				maxReruns = Integer.parseInt(val);
			} else {
				System.err.println(errorMsg);
				System.exit(0);
			}
		}
		String usedConfig = "Running the sudoku with the following parameters: \n";
		usedConfig += "sudokuFileName: " + sudokuFileName + "\n";
		usedConfig += "populationCount: " + populationCount + "\n";
		usedConfig += "maxIterations: " + maxIterations + "\n";
		usedConfig += "numberOfElites: " + numberOfElites + "\n";
		usedConfig += "tournamentSize: " + tournamentSize + "\n";
		usedConfig += "mutationProb: " + mutationProb + "\n";
		usedConfig += "maxReruns: " + maxReruns + "\n";

		System.out.println(usedConfig);

		Calendar cal = Calendar.getInstance();
		String fileName = cal.get(Calendar.HOUR_OF_DAY) + ":"
				+ cal.get(Calendar.MINUTE) + ":" + cal.get(Calendar.SECOND);
		LoggerConfigurer.configure(logger,
				Files.getNameWithoutExtension(fileName));
		List<List<Integer>> squaresMapping = SudokuReader.read(sudokuFileName);
		Individual targetIndividual = new Individual(squaresMapping);
		int solutionFoundCounter = 0;
		List<Integer> iterationsConsumed = Lists.newArrayList();
		for (int rerunCounter = 0; rerunCounter < maxReruns; rerunCounter++) {
			Population population = new Population(targetIndividual,
					populationCount);
			Random mutationRandom = new Random(System.nanoTime());
			Random generalRandom = new Random(System.nanoTime());
			int sameFitness = 0;
			double prevFitness = -1;
			int maxMutationSetCounter = 0;
			int iteration = 0;
			for (; !population.getPopulationFittestIndividual().getFitness()
					.isConverged()
					&& iteration < maxIterations; iteration++) {
				logger.info(iteration
						+ ", "
						+ population.getPopulationFittestIndividual()
								.getFitness().getOverallFitness());
				double bestFitness = population
						.getPopulationFittestIndividual().getFitness()
						.getOverallFitness();
				System.out.println("   ITERATION: "
						+ iteration
						+ ", fitness: "
						+ population.getPopulationFittestIndividual()
								.getFitness());
				System.out.println("   AVG. Population fitness: "
						+ population.getAverageFitness());
				if (bestFitness == prevFitness) {
					sameFitness++;
				} else {
					prevFitness = bestFitness;
					sameFitness = 0;
				}
				double mutation = mutationProb;
				if (sameFitness > 100) {
					maxMutationSetCounter = 20;
					sameFitness = 0;
				}
				List<Individual> fittestCandidates = population
						.getFittest(numberOfElites);

				if (useMutationExplosion && maxMutationSetCounter > 0) {
					System.out.println("RERUN");
					population = new Population(targetIndividual,
							populationCount);
					maxMutationSetCounter = 0;
					if (false) {
						System.out.println("increasing mutation");
						mutation = maxMutateProb;
						maxMutationSetCounter = Math.max(0,
								--maxMutationSetCounter);
						System.out.println("mutationSetCounter: "
								+ maxMutationSetCounter);

					}
				}
				Population newPopulation = new Population(fittestCandidates);
				System.out.println("newPopulation fitness "
						+ newPopulation.getPopulationFittestIndividual()
								.getFitness());
				while (newPopulation.getSize() < population.getSize()) {
					AbstractSelectionOperator selection;
					if (generalRandom.nextInt(2) == 0 && false) {
						selection = new RouletteWheelSelection(population);
					} else {
						selection = new TournamentSelection(population,
								tournamentSize);
					}
					Individual parent1 = selection.select(null);
					Individual parent2 = selection.select(parent1);

					AbstractCrossover crossoverOperator;
					int crossoverRandom = generalRandom.nextInt(3);
					if (crossoverRandom == 0) {
						crossoverOperator = new UniformCrossover(parent1,
								parent2);
					} else if (crossoverRandom == 1) {
						crossoverOperator = new OnePointCrossover(parent1,
								parent2);
					} else {
						crossoverOperator = new BlockwiseCrossover(parent1,
								parent2);
					}
					// Always using the onepointcrossover, it is the most
					// effective
					crossoverOperator = new OnePointCrossover(parent1, parent2);
					List<Individual> children = crossoverOperator.crossover();

					for (Individual child : children) {
						if (mutationRandom.nextInt(100) < mutation) {
							child.mutate();
						}
					}
					newPopulation.addCandidates(children);

				}
				population = newPopulation;
			}
			Individual fittest = population.getPopulationFittestIndividual();
			System.out.println("\n\n" + usedConfig);
			String msg = "Target: ";
			logger.info(msg);
			System.out.println(msg);
			msg = targetIndividual.toHumanReadable();
			logger.info(msg);
			System.out.println(msg);
			boolean solutionFound = population.getPopulationFittestIndividual()
					.getFitness().isConverged();
			msg = "Solution found? "
					+ String.valueOf(solutionFound).toUpperCase();
			msg = "Run: " + rerunCounter;
			logger.info(msg);
			System.out.println(msg);
			if (solutionFound) {
				solutionFoundCounter++;
				msg = fittest.toHumanReadable();
				logger.info(msg);
				System.out.println(msg);
				break;
			}
			iterationsConsumed.add(iteration);
		}
		logger.info("Total solutions found:" + solutionFoundCounter
				+ " out of " + maxReruns);
		logger.info("Iterations per rerun: " + iterationsConsumed);
	}
}
