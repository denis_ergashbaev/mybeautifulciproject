package core;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

public class Square {
	private static final int UNSET_NUMBER = 0;
	private List<Integer> cells;
	private List<Integer> notInitializedValueIndices = Lists.newArrayList();
	private List<Integer> initializedValues = Lists.newArrayList();

	public Square(List<Integer> values) {
		addCells(values);
		for (int i = 0; i < cells.size(); i++) {
			Integer cell = cells.get(i);
			if (cell == UNSET_NUMBER) {
				notInitializedValueIndices.add(i);
			} else {
				initializedValues.add(cell);
			}
		}
	}

	public Square(Square square) {
		addCells(square.cells);
		notInitializedValueIndices = square.notInitializedValueIndices;
		initializedValues = square.initializedValues;
	}

	private void addCells(List<Integer> values) {
		cells = Lists.newArrayList();
		for (Integer value : values) {
			cells.add(value);
		}

	}

	public void mutateUnset() {

		List<Integer> complementValues = getPossibleNumbers();
		complementValues.removeAll(initializedValues);
		Collections.shuffle(complementValues, new Random(System.nanoTime()));

		for (Integer unsetValueIndex : notInitializedValueIndices) {
			Integer replacement = complementValues.get(0);
			complementValues.remove(0);
			cells.set(unsetValueIndex, replacement);
		}
	}

	public List<Integer> getCells() {
		return cells;
	}

	public List<Integer> getPossibleNumbers() {
		List<Integer> possibleNumbers = Lists.newArrayList();
		for (int i = 1; i <= cells.size(); i++) {
			possibleNumbers.add(i);
		}
		return possibleNumbers;
	}

	public void mutate() {
		mutateUnset();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cells == null) ? 0 : cells.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Square other = (Square) obj;
		if (cells == null) {
			if (other.cells != null)
				return false;
		} else if (!cells.equals(other.cells))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return cells.toString().replace("[", "").replace("]", "");
	}

}
