package core;

import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.math.BigIntegerMath;

public class Individual {
	private final List<Square> squares = Lists.newArrayList();
	private Fitness fitness;

	public Individual(List<List<Integer>> mapping) {
		for (List<Integer> row : mapping) {
			squares.add(new Square(row));
		}
	}

	public Individual(Individual individual) {
		addSqares(individual.squares);
		for (Square square : squares) {
			square.mutateUnset();
		}
	}

	public Individual(List<Square> squares1, List<Square> squares2) {
		addSqares(squares1);
		addSqares(squares2);
	}

	public void mutate() {
		Random r = new Random(System.nanoTime());
		for (int i = 0; i < 2; i++) {
			int index = r.nextInt(squares.size());
			squares.get(index).mutate();
		}
		fitness = calculateFitness();
	}

	private void addSqares(List<Square> squares) {
		for (Square square : squares) {
			Square newSquare = new Square(square);
			this.squares.add(newSquare);
		}
	}

	public Individual produceCandidate() {
		Individual candidate = new Individual(this);
		return candidate;
	}

	public Fitness getFitness() {
		if (fitness == null) {
			fitness = calculateFitness();
		}
		return fitness;
	}

	public Fitness calculateFitness() {
		List<Integer> possibleNumbersList = squares.get(0).getPossibleNumbers();
		int possibleNumbers = possibleNumbersList.size();
		int targetArithmeticSum = possibleNumbers * (possibleNumbers + 1) / 2;
		int targetProduct = BigIntegerMath.factorial(possibleNumbers)
				.intValue();

		List<Integer> allCells = Lists.newArrayList();
		for (Square square : squares) {
			allCells.addAll(square.getCells());
		}
		List<List<Integer>> rows = SquareLineConverter.convert(allCells);

		// TODO more generic

		List<Integer> arithmeticSumRows = Lists.newArrayList();
		List<Double> geometricProductRows = Lists.newArrayList();
		int numMissingCols = 0;
		int numsMissingRows = 0;

		for (List<Integer> row : rows) {
			int arSum = 0;
			int prod = 1;
			for (Integer cell : row) {
				arSum += cell;
				prod *= cell;
			}
			arithmeticSumRows.add(Math.abs(arSum - targetArithmeticSum));
			geometricProductRows.add(Math.sqrt(Math.abs(prod - targetProduct)));
			Set<Integer> set = new HashSet<Integer>(possibleNumbersList);
			set.removeAll(row);
			numsMissingRows += set.size();
		}
		List<Integer> arithmeticSumCols = Lists.newArrayList();
		List<Double> geometricProductCols = Lists.newArrayList();
		for (int i = 0; i < rows.get(0).size(); i++) {
			Set<Integer> set = new HashSet<Integer>(possibleNumbersList);
			List<Integer> present = Lists.newArrayList();
			for (List<Integer> row : rows) {
				present.add(row.get(i));
			}
			int arSum = 0;
			for (Integer value : present) {
				arSum += value;
			}
			int geomProd = 1;
			for (Integer value : present) {
				geomProd *= value;
			}

			arithmeticSumCols.add(Math.abs(arSum - targetArithmeticSum));
			geometricProductCols.add(Math.sqrt(Math.abs(geomProd
					- targetProduct)));
			set.removeAll(present);
			numMissingCols += set.size();
		}

		return new Fitness(arithmeticSumRows, arithmeticSumCols,
				geometricProductRows, geometricProductCols, numsMissingRows,
				numMissingCols);
	}

	public List<Square> getSquares() {
		return squares;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fitness == null) ? 0 : fitness.hashCode());
		result = prime * result + ((squares == null) ? 0 : squares.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Individual other = (Individual) obj;
		if (fitness == null) {
			if (other.fitness != null)
				return false;
		} else if (!fitness.equals(other.fitness))
			return false;
		if (squares == null) {
			if (other.squares != null)
				return false;
		} else if (!squares.equals(other.squares))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Individual [\n");
		for (Square square : squares) {
			sb.append(square + "\n");
		}
		sb.append("]");
		return sb.toString();
	}

	public String toHumanReadable() {
		List<Integer> allSquares = Lists.newArrayList();
		for (Square square : squares) {
			allSquares.addAll(square.getCells());
		}
		List<List<Integer>> squares = SquareLineConverter.convert(allSquares);

		StringBuilder sb = new StringBuilder();
		for (List<Integer> square : squares) {
			sb.append(square).append("\n");
		}
		return sb.toString();
	}

}
