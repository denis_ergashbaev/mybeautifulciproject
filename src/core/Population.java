package core;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.common.collect.Lists;

public class Population {

	private static final Comparator<Individual> FITNESS_COMPARATOR = new Comparator<Individual>() {
		@Override
		public int compare(Individual o1, Individual o2) {
			Fitness f1 = o1.getFitness();
			Fitness f2 = o2.getFitness();
			if (f1.getOverallFitness() < f2.getOverallFitness()) {
				return -1;
			}
			if (f1.getOverallFitness() < f2.getOverallFitness()) {
				return 1;
			}
			return 0;
		}
	};

	private final List<Individual> candidates = Lists.newArrayList();
	private List<Individual> sortedCandidates = null;
	private double fitnessSum = -1;

	public Population(Individual targetIndividual, int populationCount) {
		for (int i = 0; i < populationCount; i++) {
			Individual newCandidate = targetIndividual.produceCandidate();
			candidates.add(newCandidate);
		}
	}

	public Population(List<Individual> candidates) {
		this.candidates.addAll(candidates);
	}

	public void addCandidates(List<Individual> candidates) {
		this.candidates.addAll(candidates);
	}

	public List<Individual> getCandidates() {
		return candidates;
	}

	public List<Individual> getFittest(int size) {
		if (size == 0 || size > this.candidates.size()) {
			return Lists.newArrayList();
		}
		List<Individual> fittestCandidates = Lists
				.newArrayList(this.candidates);
		Collections.sort(fittestCandidates, FITNESS_COMPARATOR);
		return fittestCandidates.subList(0,
				Math.min(fittestCandidates.size(), size));
	}

	public Individual getPopulationFittestIndividual() {
		List<Individual> individual = getFittest(1);
		return (individual.isEmpty()) ? null : individual.get(0);
	}

	public List<Individual> getSortedCandidates() {
		if (sortedCandidates == null) {
			sortedCandidates = candidates;
			Collections.sort(sortedCandidates, FITNESS_COMPARATOR);
		}
		return sortedCandidates;
	}

	public double getFitnessSum() {
		if (fitnessSum == -1) {
			double fitnessSum = 0;
			for (Individual individual : candidates) {
				fitnessSum += individual.getFitness().getOverallFitness();
			}
			this.fitnessSum = fitnessSum;
		}
		return fitnessSum;
	}

	public double getAverageFitness() {
		double avgFitness = 0;
		for (Individual candidate : candidates) {
			avgFitness += candidate.getFitness().getOverallFitness();
		}
		avgFitness = avgFitness / candidates.size();
		return avgFitness;
	}

	public int getSize() {
		return candidates.size();
	}

}
