package logging;

import java.io.File;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class LoggerConfigurer {

	public static void configure(Logger logger, String fileName) {
		try {
			// http://stackoverflow.com/questions/2533227/how-can-i-disable-the-default-console-handler-while-using-the-java-logging-api
			LogManager.getLogManager().reset();
			for (Handler handler : logger.getHandlers()) {
				logger.removeHandler(handler);
			}
			for (Handler handler : logger.getParent().getHandlers()) {
				logger.removeHandler(handler);
			}

			logger.setUseParentHandlers(false);

			Formatter myFormatter = new Formatter() {
				@Override
				public String format(LogRecord record) {
					return record.getMessage() + "\n";
				}

			};

			final FileHandler fh = new FileHandler("logs" + File.separator
					+ fileName + ".log");
			fh.setFormatter(myFormatter);
			logger.addHandler(fh);

			final ConsoleHandler ch = new ConsoleHandler();
			ch.setFormatter(myFormatter);
			logger.addHandler(ch);

			final String lines = "=============================================";
			logger.info(lines);
			logger.info("         " + fileName.toUpperCase());
			logger.info(lines);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
