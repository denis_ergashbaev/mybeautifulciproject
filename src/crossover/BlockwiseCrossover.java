package crossover;

import java.util.List;

import com.google.common.collect.Lists;

import core.Individual;

public class BlockwiseCrossover extends AbstractCrossover {

	public BlockwiseCrossover(Individual parent1, Individual parent2) {
		super(parent1, parent2);
	}

	@Override
	public List<Individual> crossover() {
		Individual child1 = new Individual(parent1);
		for (int i = 0; i < 3; i++) {
			int par1Fitness = parent1.getFitness()
					.getArithmeticSumRowsSegment().get(i);
			int par2Fitness = parent2.getFitness()
					.getArithmeticSumRowsSegment().get(i);
			Individual donor = par1Fitness < par2Fitness ? parent1 : parent2;
			for (int j = 0; j < 3; j++) {
				int index = i * 3 + j;
				child1.getSquares().set(index, donor.getSquares().get(index));
			}
		}

		Individual child2 = new Individual(parent2);
		for (int i = 0; i < 3; i++) {
			int par1Fitness = parent1.getFitness()
					.getArithmeticSumColsSegment().get(i);
			int par2Fitness = parent2.getFitness()
					.getArithmeticSumColsSegment().get(i);
			Individual donor = par1Fitness < par2Fitness ? parent1 : parent2;
			for (int j = 0; j < 3; j++) {
				int index = i * 3 + j;
				child1.getSquares().set(index, donor.getSquares().get(index));
			}
		}

		return Lists.newArrayList(child1, child2);
	}

}
