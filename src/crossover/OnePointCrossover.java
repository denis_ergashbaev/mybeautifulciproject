package crossover;

import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

import core.Individual;

public class OnePointCrossover extends AbstractCrossover {

	public OnePointCrossover(Individual parent1, Individual parent2) {
		super(parent1, parent2);
	}

	@Override
	public List<Individual> crossover() {
		// -1 so that there is always a split
		int cutpoint = new Random(System.nanoTime()).nextInt(parent1
				.getSquares().size() - 1);

		Individual child1 = new Individual(parent1.getSquares().subList(0,
				cutpoint), parent2.getSquares().subList(cutpoint,
				parent2.getSquares().size()));

		Individual child2 = new Individual(parent2.getSquares().subList(0,
				cutpoint), parent1.getSquares().subList(cutpoint,
				parent1.getSquares().size()));

		return Lists.newArrayList(child1, child2);
	}

}
