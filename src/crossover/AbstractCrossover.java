package crossover;

import java.util.List;

import core.Individual;

public abstract class AbstractCrossover {

	protected final Individual parent1;
	protected final Individual parent2;

	public AbstractCrossover(Individual parent1, Individual parent2) {
		this.parent1 = parent1;
		this.parent2 = parent2;
	}

	public abstract List<Individual> crossover();
}
