package crossover;

import java.util.List;
import java.util.Random;

import com.google.common.collect.Lists;

import core.Individual;
import core.Square;

public class UniformCrossover extends AbstractCrossover {

	public UniformCrossover(Individual parent1, Individual parent2) {
		super(parent1, parent2);
	}

	@Override
	public List<Individual> crossover() {
		Random r = new Random(System.nanoTime());

		Individual child1 = new Individual(parent1);
		Individual child2 = new Individual(parent2);

		int cellsSize = parent1.getSquares().get(0).getCells().size();
		for (int i = 0; i < parent1.getSquares().size(); i++) {
			Square child1Square = child1.getSquares().get(i);
			Square child2Square = child2.getSquares().get(i);
			Square parent1Square = parent1.getSquares().get(i);
			Square parent2Square = parent2.getSquares().get(i);
			for (int j = 0; j < cellsSize; j++) {
				if (r.nextInt(2) == 0) {
					child1Square.getCells().set(j,
							parent1Square.getCells().get(j));
					child2Square.getCells().set(j,
							parent2Square.getCells().get(j));
				} else {
					child1Square.getCells().set(j,
							parent2Square.getCells().get(j));
					child2Square.getCells().set(j,
							parent1Square.getCells().get(j));
				}
			}
		}
		return Lists.newArrayList(child1, child2);
	}

}
